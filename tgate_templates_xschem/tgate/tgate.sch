v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 220 -100 220 -80 {
lab=EN}
N 120 -290 190 -290 {
lab=I}
N 120 -290 120 -140 {
lab=I}
N 120 -140 190 -140 {
lab=I}
N 250 -140 320 -140 {
lab=O}
N 320 -290 320 -140 {
lab=O}
N 250 -290 320 -290 {
lab=O}
N 220 -360 220 -330 {
lab=ENB}
N 220 -80 220 -70 {
lab=EN}
N 80 -210 120 -210 {
lab=I}
N 320 -210 360 -210 {
lab=O}
N 220 -180 220 -140 {
lab=VSS}
N 220 -290 220 -240 {
lab=VDD}
N 220 -190 220 -180 {
lab=VSS}
N 220 -200 220 -190 {
lab=VSS}
N 220 -240 220 -230 {
lab=VDD}
N 490 -140 490 -120 {
lab=VSS}
N 490 -90 540 -90 {
lab=VSS}
N 420 -90 450 -90 {
lab=VSS}
N 490 -60 490 -30 {
lab=VSS}
N 540 -90 550 -90 {
lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 220 -120 1 1 {name=XN0
w=4
l=18n
nf=2
model=nmos4_lvt
spiceprefix=X
}
C {devices/iopin.sym} 220 -70 0 0 {name=p1 lab=EN}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 220 -310 3 1 {name=XP0
w=10
l=18n
nf=2
model=pmos4_lvt
spiceprefix=X
}
C {devices/iopin.sym} 220 -360 0 0 {name=p2 lab=ENB}
C {devices/iopin.sym} 80 -210 2 0 {name=p3 lab=I}
C {devices/iopin.sym} 360 -210 0 0 {name=p4 lab=O}
C {devices/iopin.sym} 120 -370 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 120 -350 2 0 {name=p6 lab=VSS}
C {devices/lab_pin.sym} 220 -200 0 0 {name=l1 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 220 -230 2 0 {name=l2 sig_type=std_logic lab=VDD}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 470 -90 0 0 {name=XDUM
w=4
l=18n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 490 -140 2 0 {name=l3 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 420 -90 0 0 {name=l4 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 490 -30 0 0 {name=l5 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 550 -90 2 0 {name=l6 sig_type=std_logic lab=VSS}
